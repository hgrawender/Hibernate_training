import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;


@Entity
@Table(name = "clientone")
public class ClientOneToOne
{         ////////////PART [13]
    @Id
    @GeneratedValue(generator = "incrementator")
    @GenericGenerator(name = "incrementator", strategy = "increment")
    private long id;

    private String firstName;
    private String lastName;
    private double salary;
    private double tax;
    //trigger for adding tax, has to be created in database earlier, comment 'create' in persistence
    //make only Getter, since no need for setter
    /*
    CREATE TRIGGER calculate_tax
    BEFORE INSERT ON clientone FOR EACH ROW
    SET new.tax = new.salary * 0.20;
    */

    @OneToOne
    @JoinColumn(name = "ID_ADRESYone")
    private Address address;



    @Override
    public String toString() {
        return "***Client specification:\n"
                    +"    First name: "+ getFirstName()
                    +"\n    Last name: "+ getLastName()
                    +"\n    Salary: "+ getSalary()+"PLN"
                    +"\n    Tax: "+ getTax();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public double getTax() {
        return tax;
    }
}
