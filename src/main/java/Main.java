import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class Main {

    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("myDatabaseUnit");
        //the same as the name unit in persistance.xml
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        //ArrayList<Object> object = new ArrayList<Object>();

        String answer = "n";
        Client client = new Client();
        //client.setId(2); //needless, since client also have auto-increment
        client.setFirstName("Robert");
        client.setLastName("Maszkiewicz");
        client.setEmail("rmaszkiewicz@gmail.com");
        client.setBalance(new BigDecimal("12342.50"));
        entityManager.getTransaction().begin();
        entityManager.persist(client);
        entityManager.getTransaction().commit();

//        List<Bank> BanksList = new ArrayList<Bank>();
//        while(answer.equals("y")){
//            System.out.println("Hi! Please give a name of bank that you want to add: ");
//            Scanner sc = new Scanner(System.in);
//            String b_name = sc.nextLine();
//            Bank bank = new Bank();
//            BanksList.add(bank);
//            bank.setName(b_name);
//            entityManager.getTransaction().begin();
//            entityManager.persist(bank);
//            entityManager.getTransaction().commit();
//            System.out.println("\n Thanks! Continue? y/n\n");
//            answer = sc.nextLine();
//
//        }

        //Klasa klasa = new Klasa();
        //klasa.setId(2);
        //klasa.setName("bob_lol");



//        for (Bank b : BanksList){
//            entityManager.persist(b);
//        }

        //entityManager.persist(klasa);


        entityManager.close();
        entityManagerFactory.close();

    }
}
