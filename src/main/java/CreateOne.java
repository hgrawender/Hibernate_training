import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class CreateOne {

    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("myDatabaseUnit");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        //REMEMBER ABOUT REFRESH AND PERSIST ORDER
        //DO NOT FLUSH DB TO PERSIST TRIGGERS

        ClientOneToOne clientone = new ClientOneToOne();
        Address address = new Address();
        clientone.setFirstName("Marek");
        clientone.setLastName("Chojecki");
        clientone.setSalary(1300.00);
        clientone.setAddress(address);
        address.setLocality("Lodz");
        address.setStreet("Pojezierska");
        address.setStreetNumber(24);
        address.setZipCode("93-223");

        entityManager.getTransaction().begin();
        entityManager.persist(address);
        entityManager.persist(clientone);
        entityManager.getTransaction().commit();

        System.out.println(clientone.toString());
        System.out.println("/////////// Before refresh ///////////");

        entityManager.refresh(address);
        entityManager.refresh(clientone);
        System.out.println("////////// After refresh ////////");
        System.out.println(clientone.toString());



        entityManager.close();
        entityManagerFactory.close();
    }
}
