import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DeleteOne {

    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("myDatabaseUnit");
        EntityManager entityManager = entityManagerFactory.createEntityManager();


        ClientOneToOne clientone = new ClientOneToOne();
        clientone.setFirstName("MAciek");
        clientone.setLastName("Morawiecki");
        clientone.setSalary(1300.00);


        entityManager.getTransaction().begin();
        entityManager.persist(clientone);
        entityManager.getTransaction().commit();

        System.out.println("/////////// Before delete ///////////");
        System.out.println(clientone.toString());

        entityManager.getTransaction().begin();
        entityManager.remove(clientone);
        entityManager.getTransaction().commit();

        System.out.println("/////////// After delete ///////////");
        System.out.println(clientone.toString());

        entityManager.close();
        entityManagerFactory.close();
    }
}
