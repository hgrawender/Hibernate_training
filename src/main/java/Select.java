import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

public class Select {

    private static EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("myDatabaseUnit");
    private static EntityManager entityManager = entityManagerFactory.createEntityManager();

    public static void main(String[] args) {



//        ClientOneToOne clientone = new ClientOneToOne();
//        Address address = new Address();
//        clientone.setFirstName("Marek");
//        clientone.setLastName("Chojecki");
//        clientone.setSalary(1300.00);
//        clientone.setAddress(address);
//        address.setLocality("Lodz");
//        address.setStreet("Pojezierska");
//        address.setStreetNumber(24);
//        address.setZipCode("93-223");

//        entityManager.refresh(address);
//        entityManager.refresh(clientone);

        addClients();
        /*ZAPYTANIE DLA SINGLE, używamy createQuery(String, <T>), bo wiemy że dostaniemy client class

            TypedQuery<ClientOneToOne> query = entityManager.createQuery("select c from ClientOneToOne c where c.lastName = 'Wielebna'", ClientOneToOne.class);
            //W zapytaniu używaj nazwy encji! a nie tabel z bazy danych!
            ClientOneToOne client = query.getSingleResult();
        */

        // ZAPYTANIE DLA ResultList
            //TypedQuery<ClientOneToOne> query = entityManager.createQuery("select c from ClientOneToOne c where c.salary > 3000", ClientOneToOne.class);
            //Parametry:, można również przekazać całą listę jako param
            TypedQuery<ClientOneToOne> query = entityManager.createQuery("select c from ClientOneToOne c where c.salary > :minSalary", ClientOneToOne.class);
            query.setParameter("minSalary", 10000.0);
            List<ClientOneToOne> clientList = query.getResultList();
            for(ClientOneToOne client : clientList){
                System.out.println(client.toString());
            }

        //Zapytanie, gdzie nie wiemy jaki obiekt będzie zwrócony
            Query query2 = entityManager.createQuery("select concat(c.firstName, ' ', c.lastName), c.salary *0.2 from ClientOneToOne c");
            Iterator<?> iterator = query2.getResultList().iterator();
            while (iterator.hasNext()){
                Object[] item = (Object[]) iterator.next();
                String fname = (String) item[0];
                Double tax =  (Double) item[1];
                System.out.println(fname + " has to pay " + tax);
            }

        entityManager.close();
        entityManagerFactory.close();
    }

    private static void addClients(){
        addClient("Maciek", "Kalaszynski", 1400.00);
        addClient("Ola", "Wielebna", 3250.00);
        addClient("Anna", "Maledna", 6500.00);
        addClient("Blazej", "Makowski", 2200.00);
        addClient("Karolina", "Farynska", 4500.00);
        addClient("Kamila", "Platowska", 11000.50);
    }


    private static void addClient(String firstName, String lastName, double balance){
        ClientOneToOne clientone = new ClientOneToOne();
        clientone.setFirstName(firstName);
        clientone.setLastName(lastName);
        clientone.setSalary(balance);
        System.out.println(clientone.toString());
        entityManager.getTransaction().begin();
        entityManager.persist(clientone);
        entityManager.getTransaction().commit();

    }
}
