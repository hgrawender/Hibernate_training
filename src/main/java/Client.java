import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
//import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "client_table")
@SecondaryTable(name = "addresses", pkJoinColumns = @PrimaryKeyJoinColumn(name = "customerid"))
public class Client {
    ////////////PART [11]
    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "incrementator")
    @GenericGenerator(name = "incrementator", strategy = "increment")
    private Integer id;
    @Column(name = "firstname", nullable = false, length = 20)
    private String firstName;
    @Column(name = "lastname", columnDefinition = "VARCHAR(40) NOT NULL")
    private String lastName;
    @Column(table = "addresses", columnDefinition = "VARCHAR(40)")
    private String email;
    @Column(precision = 10, scale = 2)
    private BigDecimal balance;
    @Column
    private int[] integerArray;
    @Transient                      //ignore this in Hibernate
    private int ignoredField;
    @Temporal(TemporalType.TIMESTAMP)
    private java.util.Date timestamp;
    //private java.sql.Timestamp timestamp;       //wrapper on java.util.Date



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public int[] getIntegerArray() {
        return integerArray;
    }

    public void setIntegerArray(int[] integerArray) {
        this.integerArray = integerArray;
    }

    public int getIgnoredField() {
        return ignoredField;
    }

    public void setIgnoredField(int ignoredField) {
        this.ignoredField = ignoredField;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
}
