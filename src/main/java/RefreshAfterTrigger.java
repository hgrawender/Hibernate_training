import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.math.BigDecimal;

public class RefreshAfterTrigger {


    public static void main(String[] args) {

        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("myDatabaseUnit");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();

        Client modClient = entityManager.find(Client.class, 1);

        System.out.println("First name: " + modClient.getFirstName());
        System.out.println("Last name: " + modClient.getLastName());
        System.out.println("Balance: " + modClient.getBalance() + "PLN");
        modClient.setBalance(new BigDecimal("2000.00"));

        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();
    }
}


